module.exports = {
  projects: [
    '<rootDir>/apps/retro-web',
    '<rootDir>/apps/retro-ionic',
    '<rootDir>/libs/retro/web/feature',
    '<rootDir>/libs/retro/web/ui',
    '<rootDir>/libs/retro/web/boards/feature',
    '<rootDir>/libs/shared/ui-icons',
    '<rootDir>/libs/shared/data-access-auth',
  ],
};
