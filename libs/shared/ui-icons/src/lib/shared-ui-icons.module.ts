import { ModuleWithProviders, NgModule } from '@angular/core';
import { IconPipe, RETRO_ICONS } from './icon.pipe';

@NgModule({
  declarations: [IconPipe],
  exports: [IconPipe],
})
export class SharedUiIconsModule {
  static forRoot(icons: {
    [key: string]: string;
  }): ModuleWithProviders<SharedUiIconsModule> {
    return {
      ngModule: SharedUiIconsModule,
      providers: [{ provide: RETRO_ICONS, useValue: icons }],
    };
  }

  static forFeature(icons: {
    [key: string]: string;
  }): ModuleWithProviders<SharedUiIconsModule> {
    return {
      ngModule: SharedUiIconsModule,
      providers: [
        {
          provide: RETRO_ICONS,
          deps: [RETRO_ICONS],
          useFactory: (fromRoot: { [key: string]: string }) =>
            icons
              ? {
                  ...fromRoot,
                  ...icons,
                }
              : fromRoot,
        },
      ],
    };
  }
}
