import {
  InjectionToken,
  PipeTransform,
  Optional,
  Inject,
  Pipe,
} from '@angular/core';
import { propOr } from 'ramda';

export const RETRO_ICONS = new InjectionToken<{ [key: string]: string }>(
  'RETRO_ICONS'
);

@Pipe({ name: 'icon' })
export class IconPipe implements PipeTransform {
  constructor(
    @Optional()
    @Inject(RETRO_ICONS)
    private readonly _icons?: { [key: string]: string }
  ) {}

  transform(value: string) {
    if (this._icons && value) {
      return propOr(value, value, this._icons);
    }
    return value;
  }
}
