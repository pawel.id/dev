import { merge, Observable, Subject } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';

import { AuthService } from './auth.service';

export class AuthProxy {
  private readonly _signin$ = new Subject();
  private readonly _login$ = new Subject();
  private readonly _logout$ = new Subject();

  readonly authenticated$: Observable<string>;

  constructor(private readonly _authService: AuthService) {
    const signin$ = this._signin$.pipe(
      switchMap(() => this._authService.signin())
    );
    const login$ = this._login$.pipe(
      switchMap(() => this._authService.login())
    );
    const logout$ = this._logout$.pipe(
      switchMap(() => this._authService.logout())
    );
    this.authenticated$ = merge(signin$, login$, logout$).pipe(shareReplay(1));
  }

  signin() {
    this._signin$.next();
  }

  login() {
    this._login$.next();
  }

  logout() {
    this._logout$.next();
  }
}
