import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor() {}

  signin(): Observable<string> {
    return of('token=MOCK_TOKEN');
  }

  login(): Observable<string> {
    return of('token=MOCK_TOKEN');
  }

  logout(): Observable<string> {
    return of(null);
  }
}
