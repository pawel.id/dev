import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { TopbarComponent } from './topbar.component';
import { TopbarModule } from './topbar.module';

storiesOf('topbar', module)
  .addDecorator(
    moduleMetadata({
      imports: [TopbarModule, MatButtonModule, MatIconModule],
    })
  )
  .add('default', () => ({
    component: TopbarComponent,
    props: {},
    template: `
        <dev-topbar>
            <span>Default header</span>
            <div devTopbarLeft>
                <button mat-icon-button><mat-icon>menu</mat-icon></button>
            </div>
            <div devTopbarRight>
                <button mat-icon-button><mat-icon>notifiaction</mat-icon></button>
                <button mat-icon-button><mat-icon>more_vert</mat-icon></button>
            </div>
        </dev-topbar>
    `,
  }));
