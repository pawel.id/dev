import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopbarModule } from './topbar/topbar.module';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  exports: [TopbarModule],
})
export class RetroWebUiModule {}
