import { Component } from '@angular/core';

const MENU_LINKS: { text: string; link: any; icon: string }[] = [];

@Component({
  selector: 'dev-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss'],
})
export class CoreComponent {
  items = MENU_LINKS;
}
