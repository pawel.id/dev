import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SharedUiIconsModule } from '@dev/shared/ui-icons';
import { TopbarModule } from '@dev/retro/web/ui';

import { CoreComponent } from './core.component';

const material = [
  MatSidenavModule,
  MatTooltipModule,
  MatIconModule,
  MatListModule,
];

@NgModule({
  declarations: [CoreComponent],
  imports: [
    CommonModule,
    RouterModule,
    TopbarModule,
    SharedUiIconsModule,
    ...material,
  ],
  exports: [CoreComponent],
})
export class CoreModule {}
