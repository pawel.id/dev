import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardComponent } from './board/board.component';
import { RoutingModule } from './routing.module';

@NgModule({
  imports: [CommonModule, RoutingModule],
  declarations: [BoardComponent],
})
export class RetroWebBoardsFeatureModule {}
